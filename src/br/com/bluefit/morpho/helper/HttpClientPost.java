package br.com.bluefit.morpho.helper;


import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HttpClientPost {

	public static void sendPost(String jsonToSend) throws Exception {

		HttpPost post = new HttpPost("http://localhost:8000/");
		
		
		post.addHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
		post.setHeader("Accept", "application/json");


		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("biometric", jsonToSend));
		
	    System.out.println(jsonToSend);

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(post)) {

			System.out.println(EntityUtils.toString(response.getEntity()));
		}
		

	}
	
	
	public static void sendPOSTJson(JSONObject json) throws Exception {
	    CloseableHttpClient client = HttpClients.createDefault();
	    HttpPost httpPost = new HttpPost("http://localhost:5000/");
	 
	    String jsonString = json.toString();
	    
	    System.out.println(jsonString);
	    
	    StringEntity entity = new StringEntity(jsonString);
	    httpPost.setEntity(entity);
	    httpPost.setHeader("Accept", "application/json");
	    httpPost.setHeader("Content-type", "application/json");
	 
	    client.close();
	}

}
