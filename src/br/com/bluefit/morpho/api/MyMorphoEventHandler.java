package br.com.bluefit.morpho.api;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.json.JSONObject;

import br.com.bluefit.morpho.websocket.MyWebSocketServlet;
import morpho.morphosmart.sdk.api.IMorphoEventHandler;
import morpho.morphosmart.sdk.api.MorphoCallbackEnrollmentStatus;
import morpho.morphosmart.sdk.api.MorphoCommandStatus;
import morpho.morphosmart.sdk.api.MorphoImage;
import sun.misc.BASE64Encoder;

public class MyMorphoEventHandler implements IMorphoEventHandler {

	@Override
	public void onBusyWarningEvent() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCodeQualityEvent(short arg0) {
		new Thread() {
			@Override
			public void run() {
				System.out.println(arg0);
				JSONObject json = new JSONObject();
				json.put("codeQuality", arg0);
				MyWebSocketServlet.broadcast(json.toString());

			}
		}.start();
	}

	@Override
	public void onCommandStatusEvent(MorphoCommandStatus arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDetectQualityEvent(short arg0) {
//		new Thread() {
//			@Override
//			public void run() {
//				System.out.println("onDetectQualityEvent");
//				JSONObject json = new JSONObject();
//				json.put("Detect", arg0);
//				MyWebSocketServlet.broadcast(json.toString());
//
//			}
//		}.start();
//		

	}

	@Override
	public void onEnrolmentEvent(MorphoCallbackEnrollmentStatus arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onImageEvent(MorphoImage morphoImage) {
		BufferedImage bufferedImage = MyMorphoEventHandler.toBufferedImage(morphoImage.getImage(), morphoImage.getImageHeader().getNbCol(),
				morphoImage.getImageHeader().getNbRow());
		String image64 = MyMorphoEventHandler.encodeToString(bufferedImage, "jpeg");
		MyWebSocketServlet.broadcast(image64);
	}

	@Override
	public void onImageLastEvent(MorphoImage morphoImage) {

		// TODO Auto-generated method stub
	}

	public static BufferedImage toBufferedImage(final byte[] buffer, final int width, final int height) {
		BufferedImage bufferedImage = null;

		try {
			bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
			bufferedImage.setAccelerationPriority(1);
			final WritableRaster raster = bufferedImage.getRaster();
			final int[] gray = new int[1];
			int i = 0, x;
			for (int y = 0; y < height; y++) {
				for (x = 0; x < width; x++) {
					gray[0] = buffer[i++];
					raster.setPixel(x, y, gray);
				}
			}
		} catch (final NullPointerException e) {
			bufferedImage = null;
			System.out.println("error pointer");
			e.printStackTrace();
		} catch (final ArrayIndexOutOfBoundsException e) {
			bufferedImage = null;
			System.out.println("error Array");
		}
		return bufferedImage;
	}
	
	
	public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
 
        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();
 
            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);
 
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }
	
	public static BufferedImage resizeImage(BufferedImage bufferedImage, int width, int height) {
		
		int bufferedImageWidth = bufferedImage.getWidth();
		int bufferedImageHeight = bufferedImage.getHeight();
		double ratio = 0;
		int widthZM;
		int heightZM;
		
		if(
			(height >= bufferedImageHeight && width >= bufferedImageWidth) ||
			(height < bufferedImageHeight && width < bufferedImageWidth)
		)
		{
			ratio = Math.min(height/(double)bufferedImageHeight, width/(double)bufferedImageWidth);
		}
		else if (height < bufferedImageHeight)
		{
			ratio = height/(double)bufferedImageHeight;
		}
		else
		{
			ratio = width/(double)bufferedImageWidth;
		}
		
		widthZM = (int) (bufferedImageWidth * ratio);
		heightZM = (int) (bufferedImageHeight * ratio);
		
		int x = Math.abs(widthZM - width)/2;
		int y = Math.abs(heightZM - height)/2;
		
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = (Graphics2D) bi.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(bufferedImage, x, y, widthZM, heightZM, null);
        g2d.dispose();
        return bi;
    }


}
