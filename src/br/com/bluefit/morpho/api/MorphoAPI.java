package br.com.bluefit.morpho.api;

import java.util.ArrayList;
import org.json.JSONObject;


public class MorphoAPI {	
	
	public static MyMorphoEventHandler myMorphoEventHandlerSameReference;
	
	public MorphoOperations morphoOperations = new MorphoOperations();
	
	
	public ArrayList<Byte> start(){
		ArrayList<Byte> result = morphoOperations.getArrayListByteFromFinger();
		return result;
	}
	
	
	public void closeMorpho() {
		this.morphoOperations.close();
	}
	
	
	
	
}
