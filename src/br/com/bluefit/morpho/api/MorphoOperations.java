package br.com.bluefit.morpho.api;

import java.util.ArrayList;

import morpho.morphosmart.sdk.api.IMorphoEventHandler;
import morpho.morphosmart.sdk.api.MorphoCompressAlgo;
import morpho.morphosmart.sdk.api.MorphoDatabase;
import morpho.morphosmart.sdk.api.MorphoDevice;
import morpho.morphosmart.sdk.api.MorphoFAR;
import morpho.morphosmart.sdk.api.MorphoFVPTemplateType;
import morpho.morphosmart.sdk.api.MorphoSmartSDK;
import morpho.morphosmart.sdk.api.MorphoTemplateEnvelope;
import morpho.morphosmart.sdk.api.MorphoTemplateList;
import morpho.morphosmart.sdk.api.MorphoTemplateType;
import morpho.morphosmart.sdk.api.MorphoUser;

public class MorphoOperations {
	
	
	public MorphoDevice morphoDevice;
	
	private MorphoTemplateList templateList;
	
	private IMorphoEventHandler morphoEventHandler;
	
	
	
	public void close() {
		this.morphoDevice.closeDevice();
	}
	
	
	
	public MorphoOperations() {


		try {
			System.loadLibrary("MorphoSmartSDKJavaWrapper");
			if (System.getProperty("os.name").startsWith("Windows")) {
				System.loadLibrary("MSOSECUJavaWrapper");
			}
		} catch (UnsatisfiedLinkError e) {
			System.out.print(e.toString());
			System.exit(1);
		}

		this.morphoDevice = new MorphoDevice();
		this.templateList = new MorphoTemplateList();
		this.morphoEventHandler = new MyMorphoEventHandler();
		/**
		 * esse atributo statico faz a mesma refencia pois ele que manda o status de maneira asyncrona
		 */
		MorphoAPI.myMorphoEventHandlerSameReference = 	(MyMorphoEventHandler) this.morphoEventHandler;
		
		MorphoDatabase morphoDatabase = new MorphoDatabase();
		MorphoUser morphoUser = new MorphoUser();
		
		int ret = morphoDevice.openUsbDevice("293666214-1915I008550", (long) 1000);
		
		
		System.out.println("Morpho open status: " + ret);

		if (ret == MorphoSmartSDK.MORPHO_OK) {
			ret = morphoDevice.getDatabase((short) 0, (String) null, morphoDatabase);
		}

		if (ret == MorphoSmartSDK.MORPHO_OK) {
			ret = morphoDatabase.identify(0, MorphoFAR.MORPHO_FAR_5, 0, null, morphoUser, null, null,
					MorphoSmartSDK.MORPHO_DEFAULT_CODER, MorphoSmartSDK.MORPHO_VERIF_DETECT_MODE, 0, 1);
		}
		if (ret == MorphoSmartSDK.MORPHO_OK) {
			ArrayList<String> dataField = new ArrayList<String>();
			ret = morphoUser.getField(0, dataField);
		}

	}
	
	public ArrayList<Byte> getArrayListByteFromFinger() {
		
		this.capture();
		
		MorphoTemplateType[] morphoArrayTemplateType = { MorphoTemplateType.MORPHO_PK_COMP };
		
		short[] pkQuality = { 0 };

		short[] shorta = { 0 };
		
		ArrayList<Byte> arrayFromBytesFinger = new ArrayList<Byte>();
		
		this.templateList.getTemplate((short) 0, morphoArrayTemplateType, arrayFromBytesFinger, pkQuality, shorta);
		
		System.out.println("morpho closeDevice() status " + this.morphoDevice.closeDevice()); 
		
		return arrayFromBytesFinger;
	}
	
	private void capture() {
		
		this.morphoDevice.capture((int) 30, (short) 80, (short) 0, (short) 1, MorphoTemplateType.MORPHO_PK_COMP,
				MorphoFVPTemplateType.MORPHO_NO_PK_FVP, (int) 255, (short) 0, (long) 199, this.morphoEventHandler,
				this.templateList, MorphoTemplateEnvelope.MORPHO_RAW_TEMPLATE, null, (short) 0, (short) 0,
				(long) 18,(short) 0,MorphoCompressAlgo.MORPHO_COMPRESS_BMP, (short) 0);
	}
	
	public IMorphoEventHandler getEventHandler() {
		return this.morphoEventHandler;
	}
	

}
