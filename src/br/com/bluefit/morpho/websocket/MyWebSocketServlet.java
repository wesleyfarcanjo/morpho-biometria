package br.com.bluefit.morpho.websocket;

import java.io.IOException;
import java.nio.CharBuffer;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;

@SuppressWarnings("deprecation")
@WebServlet("/websocket")
public class  MyWebSocketServlet extends WebSocketServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final ConnectionWS connections = new ConnectionWS();

	@Override
	protected StreamInbound createWebSocketInbound(String subProtocol, HttpServletRequest request) {
		//aqui e para criar o socket
		
		System.out.println(connections.estancias);

		return connections;
	}
	
	
	public static final void broadcast(String message) {
		//aqui e para enviar dados
			try {
				connections.getWsOutbound().writeTextMessage(CharBuffer.wrap(message));
			} catch (IOException ioe) {
				
			}
		}
	
	
	
	}
