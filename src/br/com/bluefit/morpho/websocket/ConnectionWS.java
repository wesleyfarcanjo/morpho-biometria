package br.com.bluefit.morpho.websocket;

import java.io.IOException;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.ArrayList;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.WsOutbound;
import org.json.JSONObject;

import br.com.bluefit.morpho.api.MorphoAPI;

@SuppressWarnings("deprecation")
public class ConnectionWS extends MessageInbound {
	
	public static int estancias;
	
	private MorphoAPI morphoAPI = null;
	
	public ConnectionWS() {

	}


	protected void onOpen(WsOutbound outbound) {
		
		System.out.println("abriu uma conexao");
		

		
		new Thread() {
			@Override
			public void run() {
				morphoAPI = new MorphoAPI();
				ArrayList<Byte> valuesFromFingers = morphoAPI.start();

				
				JSONObject json = new JSONObject();
				
				json.put("finger", valuesFromFingers.toString());

				
				try {
					MyWebSocketServlet.broadcast(json.toString());
					MyWebSocketServlet.broadcast("fechar");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}.start();
		
		
		
	}
	
	@Override
	protected void onClose(int status) {

		
	}
	@Override
	protected void onBinaryMessage(ByteBuffer message) throws IOException {
		//aqui e quando recebe array de btyes
		throw new RuntimeException("Metodo nao aceito");
		
	}

	@Override
	protected void onTextMessage(CharBuffer msg) throws IOException {

	}

}
