package br.com.bluefit.morpho.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import br.com.bluefit.morpho.api.MorphoAPI;

@WebServlet(name = "MorphoServlet", urlPatterns = { "/morpho" })

public class MainServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		MorphoAPI morphoAPI = new MorphoAPI();
		ArrayList<Byte> valuesFromFingers = morphoAPI.start();
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", valuesFromFingers);
		out.println(jsonObject.toString());

		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		MorphoAPI morphoAPI = new MorphoAPI();
		ArrayList<Byte> valuesFromFingers = morphoAPI.start();
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", valuesFromFingers);
		out.println(jsonObject.toString());
		
	}
}
